# Purpose of this project

Discover about AWS

# Installation
Install the node dependencies 
```bash
npm install
```

# Start the application
Build and up the containers
```bash
docker-composer up -d --build
```

# TODO
- Use terraform to manage AWS
- Create Jobs for coding style (eslint & prettier)
- Create Job to deploy the lambda function via GitlabCI
